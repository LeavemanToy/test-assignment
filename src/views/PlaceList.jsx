import React from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import { Avatar, Badge, Grid, Hidden, List, ListItem, ListItemButton, ListItemIcon, Menu, Paper } from "@mui/material";
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Divider from '@mui/material/Divider';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import exampleData from "../examples/example_data.json";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ListAltIcon from '@mui/icons-material/ListAlt';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import PersonAdd from '@mui/icons-material/PersonAdd';
import Settings from '@mui/icons-material/Settings';
import Logout from '@mui/icons-material/Logout';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import Carousel from 'react-material-ui-carousel'

const day_check = [
  { id: 0, day: 'Sunday' },
  { id: 1, day: 'Monday' },
  { id: 2, day: 'Tuesday' },
  { id: 3, day: 'Wednesday' },
  { id: 4, day: 'Thursday' },
  { id: 5, day: 'Friday' },
  { id: 6, day: 'Saturday' },
]

function PlaceList() {
  const d = new Date();
  var day_now = day_check.find(element => element.id === d.getDay())
  const [data, setData] = React.useState(exampleData)
  const [filterData, setFilterData] = React.useState('All');
  const [detailPlace, setDetailPlace] = React.useState([])
  const [searchText, setSearchText] = React.useState('')
  const [tempFilter, setTempFilter] = React.useState([])
  const [currentPage, setCurrentPage] = React.useState(1)
  const [totalPage, setTotalPage] = React.useState(1)
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [valueTab, setValueTab] = React.useState('1');
  const open = Boolean(anchorEl);
  const [anchorElNotificatipn, setAnchorElNotification] = React.useState(null);
  const openNotification = Boolean(anchorElNotificatipn);

  React.useEffect(() => {
    setTempFilter(data)
    splitData(currentPage - 1)
  }, [data])

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickNotification = (event) => {
    setAnchorElNotification(event.currentTarget);
  };

  const handleCloseNotification = () => {
    setAnchorElNotification(null);
  };

  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };

  const splitData = (page) => {
    let temp_data = [...data]
    if (temp_data.length > 9) {
      setCurrentPage(page + 1)
      setTotalPage(Math.ceil(data.length / 9))
    } else {
      setCurrentPage(1)
      setTotalPage(1)
    }
  }

  const handleChange = (event) => {
    let temp_data = [...exampleData]
    let new_data = []
    if (event === 'Restaurant') {
      for (let index = 0; index < temp_data.length; index++) {
        const element = temp_data[index];
        if (element.categories.find(ele => ele === event.toLowerCase()) !== undefined) {
          new_data.push(element)
        }
      }
    } else if (event === 'Bakery') {
      for (let index = 0; index < temp_data.length; index++) {
        const element = temp_data[index];
        if (element.categories.find(ele => ele === event.toLowerCase()) !== undefined) {
          new_data.push(element)
        }
      }
    } else if (event === 'Cafe') {
      for (let index = 0; index < temp_data.length; index++) {
        const element = temp_data[index];
        if (element.categories.find(ele => ele === event.toLowerCase()) !== undefined) {
          new_data.push(element)
        }
      }
    } else {
      new_data = [...exampleData]
    }
    setCurrentPage(1)
    setData(new_data)
    setFilterData(event);
    setSearchText('')
  };

  const submitSearch = (value) => {
    let temp_data = [...tempFilter]
    let new_data = []
    if (value.toLowerCase() !== '') {
      for (let index = 0; index < temp_data.length; index++) {
        const element = temp_data[index];
        if (element.name.toLowerCase().search(value.toLowerCase()) === 0) {
          new_data.push(element)
        }
      }
      setData(new_data)
    } else {
      handleChange(filterData)
    }
  }

  const handleChangePage = (event, newPage) => {
    splitData(newPage - 1)
  }

  return (
    <div>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="fixed" style={{ backgroundColor: '#134B8A' }}>
          <Toolbar style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Hidden only={'xs'}>
              <div style={{ position: 'fixed', left: 18 }}>
                <img src={require('../assets/imgs/logo.png')} width='50px' height='50px' style={{ marginRight: 'auto', borderRadius: '6px' }} />
              </div>
            </Hidden>
            <Hidden only={'xs'}>
              <Menu
                anchorEl={anchorElNotificatipn}
                id="account-menu"
                open={openNotification}
                onClose={handleCloseNotification}
                onClick={handleCloseNotification}
                PaperProps={{
                  elevation: 0,
                  sx: {
                    overflow: 'visible',
                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    mt: 1.5,
                    '& .MuiAvatar-root': {
                      width: 32,
                      height: 32,
                      ml: -0.5,
                      mr: 1,
                    },
                    '&:before': {
                      content: '""',
                      display: 'block',
                      position: 'absolute',
                      top: 0,
                      right: 14,
                      width: 10,
                      height: 10,
                      bgcolor: 'background.paper',
                      transform: 'translateY(-50%) rotate(45deg)',
                      zIndex: 0,
                    },
                  },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
              >
                <Typography gutterBottom variant="h6" component="div" style={{ padding: '5px' }}>
                  การแจ้งเตือน
                </Typography>
                <Divider />
                <MenuItem style={{ backgroundColor: '#c4d3e9', width: '250px' }}>
                  <ListItemIcon>
                    <CardGiftcardIcon fontSize="small" />
                  </ListItemIcon>
                  โปรโมชั่น 1 แถม 1
                </MenuItem>
              </Menu>
              <IconButton component="label" onClick={handleClickNotification}>
                <Badge badgeContent={1} color="error" style={{ marginRight: '10px' }}>
                  <NotificationsIcon style={{ color: '#ffffff' }} />
                </Badge>
              </IconButton>
              <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                  elevation: 0,
                  sx: {
                    overflow: 'visible',
                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    mt: 1.5,
                    '& .MuiAvatar-root': {
                      width: 32,
                      height: 32,
                      ml: -0.5,
                      mr: 1,
                    },
                    '&:before': {
                      content: '""',
                      display: 'block',
                      position: 'absolute',
                      top: 0,
                      right: 14,
                      width: 10,
                      height: 10,
                      bgcolor: 'background.paper',
                      transform: 'translateY(-50%) rotate(45deg)',
                      zIndex: 0,
                    },
                  },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
              >
                <MenuItem>
                  <Avatar /> Profile
                </MenuItem>
                <MenuItem>
                  <Avatar /> My account
                </MenuItem>
                <Divider />
                <MenuItem>
                  <ListItemIcon>
                    <PersonAdd fontSize="small" />
                  </ListItemIcon>
                  Add another account
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <Settings fontSize="small" />
                  </ListItemIcon>
                  Settings
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <Logout fontSize="small" />
                  </ListItemIcon>
                  Logout
                </MenuItem>
              </Menu>
              <Button
                style={{ color: '#ffffff' }}
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
                startIcon={<Avatar alt="Aemy Sharp" src="/static/images/avatar/1.jpg" />}
                endIcon={<KeyboardArrowDownIcon />}
              >
                Akkarapol
              </Button>
            </Hidden>
            <Hidden smUp>
              <img src={require('../assets/imgs/logo.png')} width='50px' height='50px' style={{ marginRight: 'auto', borderRadius: '6px' }} />
              <Avatar alt="Aemy Sharp" src="/static/images/avatar/1.jpg" />
            </Hidden>
          </Toolbar>
        </AppBar>
      </Box>
      <Grid container style={{ height: '100vh', marginTop: '60px' }}>
        <Hidden mdDown>
          <Grid item md={1} style={{ borderRight: '1px solid' }}>
            <List>
              <Divider />
              <ListItem disablePadding>
                <ListItemButton style={{ display: 'block' }}>
                  <ListItemIcon style={{
                    height: '45px',
                    backgroundColor: '#0f1e56',
                    color: '#ffffff',
                    borderRadius: '6px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}>
                    <ListAltIcon />
                  </ListItemIcon>
                  <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}>
                    Place
                  </div>
                </ListItemButton>
              </ListItem>
              <Divider />
            </List>
          </Grid>
        </Hidden>
        <Grid item md={11} xs={12} style={{ padding: '20px' }}>
          {detailPlace.length === 0 ?
            <Grid container>
              <Grid item md={6} xs={12}>
                <h1>
                  Place List
                </h1>
              </Grid>
              <Grid item md={6} xs={12}>
                <Grid container style={{ display: 'flex', justifyContent: 'flex-end' }}>
                  <Grid item md={5} xs={12} style={{ padding: '5px' }}>
                    <Box sx={{ minWidth: 120 }}>
                      <FormControl fullWidth>
                        <Select
                          id="demo-simple-select"
                          value={filterData}
                          onChange={(event) => handleChange(event.target.value)}
                        >
                          <MenuItem value={'All'}>All</MenuItem>
                          <MenuItem value={'Restaurant'}>Restaurant</MenuItem>
                          <MenuItem value={'Bakery'}>Bakery</MenuItem>
                          <MenuItem value={'Cafe'}>Cafe</MenuItem>
                        </Select>
                      </FormControl>
                    </Box>
                  </Grid>
                  <Divider orientation="vertical" flexItem style={{ color: '#134B8a' }} />
                  <Grid item md={6} xs={12} style={{ display: 'flex', alignItems: 'center', padding: '5px' }}>
                    <Paper
                      component="form"
                      sx={{ p: '2px 4px', display: 'flex', alignItems: 'center' }}
                      style={{ borderRadius: '50px', border: '2px solid #134B8a', width: '100%', height: '50px' }}
                    >
                      <InputBase
                        sx={{ ml: 1, flex: 1 }}
                        placeholder="Search name..."
                        inputProps={{ 'aria-label': 'search name...' }}
                        onKeyDown={(e) => e.key === 'Enter' ? submitSearch(searchText) : ''}
                        onChange={(e) => setSearchText(e.target.value)}
                        value={searchText}
                      />
                      <IconButton onClick={() => submitSearch(searchText)} type="button" sx={{ p: '10px' }} aria-label="search">
                        <SearchIcon />
                      </IconButton>
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            :
            <Grid container>
              <Button
                onClick={() => setDetailPlace([])}
                style={{
                  backgroundColor: '#134b8a',
                  borderRadius: '20px',
                  color: '#ffffff',
                  fontFamily: 'Kanit'
                }}
              >
                &lt; Back
              </Button>
            </Grid>
          }
          <Grid container style={{ color: 'yellow' }}>
            {detailPlace.length === 0 ?
              <>
                {data.length !== 0 ? data?.map((item, index) => {
                  return (
                    <React.Fragment>
                      <Grid
                        key={index}
                        item
                        md={4}
                        xs={12}
                        style={{
                          padding: '5px',
                          display: index >= (currentPage - 1) * 9 && index <= ((currentPage - 1) * 9) + 8 ? '' : 'none'
                        }}
                      >
                        <Card style={{ marginBottom: '15px' }}>
                          <CardActionArea onClick={() => setDetailPlace(item)}>
                            <Grid container>
                              <Grid item md={4} xs={12} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <Hidden mdUp>
                                  <CardMedia
                                    component="img"
                                    image={item.profile_image_url}
                                    alt="images"
                                    style={{ height: '120px', width: '100%', borderRadius: '6px' }}
                                  />
                                </Hidden>
                                <Hidden mdDown>
                                  <CardMedia
                                    component="img"
                                    image={item.profile_image_url}
                                    alt="images"
                                    style={{ height: '70px', width: '70px', borderRadius: '6px' }}
                                  />
                                </Hidden>
                              </Grid>
                              <Grid item md={8} xs={12}>
                                <CardContent>
                                  <Typography gutterBottom variant="h5" component="div">
                                    {item.name}
                                  </Typography>
                                  <Typography variant="body2" color="text.secondary">
                                    <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                                      <CalendarMonthIcon /> {item?.operation_time?.map((item_day, index_day) => {
                                        return (
                                          <span key={index_day}>
                                            {item_day.day === (day_now.day) ?
                                              <span>{item_day.time_open} AM - {item_day.time_close} PM</span>
                                              : ''}
                                          </span>
                                        )
                                      })}
                                      <span style={{ color: '#134B8a', fontWeight: 'bold', display: 'flex', justifyContent: 'center', alignItems: 'center', marginLeft: 'auto' }}>
                                        <FiberManualRecordIcon /> {item.rating}
                                      </span>
                                    </div>
                                  </Typography>
                                </CardContent>
                              </Grid>
                            </Grid>
                            <Grid container>
                              <Hidden smDown>
                                <ImageList sx={{ width: 500, height: 165 }} cols={3} rowHeight={164} style={{ padding: '10px' }}>
                                  {item?.images?.map((item_img, index_img) => (
                                    <ImageListItem key={`${item_img}${index_img}`}>
                                      <img
                                        style={{
                                          height: 164,
                                          borderTopLeftRadius: index_img === 0 ? '6px' : '',
                                          borderBottomLeftRadius: index_img === 0 ? '6px' : '',
                                          borderTopRightRadius: index_img === 2 ? '6px' : '',
                                          borderBottomRightRadius: index_img === 2 ? '6px' : ''
                                        }}
                                        src={item_img}
                                        srcSet={item_img}
                                        alt={item_img}
                                        loading="lazy"
                                      />
                                    </ImageListItem>
                                  ))}
                                </ImageList>
                              </Hidden>
                            </Grid>
                          </CardActionArea>
                          <Hidden smUp>
                            <div style={{ padding: '5px', borderRadius: '6px' }}>
                              <Carousel
                                autoPlay={false}
                                navButtonsAlwaysVisible={true}
                                height={200}
                              >
                                {item?.images?.map((item_img, index_img) => (
                                  <img
                                    id={`${item_img}${index_img}`}
                                    width='315'
                                    height='210'
                                    style={{ borderRadius: '6px' }}
                                    src={item_img}
                                    alt=""
                                  />
                                ))}
                              </Carousel>
                            </div>
                          </Hidden>
                        </Card>
                      </Grid>
                    </React.Fragment>
                  )
                }) :
                  <Card style={{ fontSize: '50px', height: '300px', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    ไม่พบข้อมูล
                  </Card>
                }
              </>
              :
              <React.Fragment>
                <Hidden smDown>
                  <Grid
                    container
                    style={{
                      backgroundColor: '#c4d3e9',
                      margin: '5px',
                      padding: '10px',
                      borderRadius: '6px'
                    }}
                  >
                    <Grid item md={6} xs={12} style={{ padding: '5px' }} >
                      <Card
                        style={{ borderRadius: '6px' }}
                      >
                        <CardMedia
                          component="img"
                          height="250"
                          image={detailPlace.profile_image_url}
                          alt="green iguana"
                        />
                        <CardContent>
                          <Grid container>
                            <Typography gutterBottom variant="h5" component="div">
                              {detailPlace.name}
                            </Typography>
                            <span style={{ color: '#134B8a', fontWeight: 'bold', display: 'flex', justifyContent: 'center', alignItems: 'center', marginLeft: 'auto' }}>
                              <FiberManualRecordIcon /> {detailPlace.rating}
                            </span>
                          </Grid>
                          <Grid container style={{ fontSize: '13px' }}>
                            <Grid item md={3} xs={12} style={{ fontSize: '13px' }}>
                              <Typography gutterBottom component="div" style={{ fontSize: '13px', fontWeight: 'bold' }}>
                                Address :
                              </Typography>
                            </Grid>
                            <Grid item md={9} xs={12} >
                              <Typography gutterBottom component="div" style={{ fontSize: '13px' }}>
                                {detailPlace.address}
                              </Typography>
                            </Grid>
                            <Grid container style={{ fontSize: '15' }}>
                              <Grid item md={3} xs={12}>
                                <Typography gutterBottom component="div" style={{ fontSize: '13px', fontWeight: 'bold' }}>
                                  Opening Hour :
                                </Typography>
                              </Grid>
                              <Grid item md={9} xs={12}>
                                <Typography gutterBottom component="div" style={{ fontSize: '13px' }}>
                                  {detailPlace.operation_time.map((item_open, index_open) => {
                                    return (
                                      <div>
                                        {item_open.day}: {item_open.time_open === 'closed' ? 'Closed' : `${item_open.time_open} AM`} {item_open.time_open === 'closed' ? '' : `- ${item_open.time_close} PM`}
                                      </div>
                                    )
                                  })}
                                </Typography>
                              </Grid>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Card>
                    </Grid>
                    <Grid item md={6} xs={12} style={{ padding: '5px' }}>
                      <Card
                        style={{
                          borderRadius: '6px',
                          padding: '15px'
                        }}
                      >
                        <Typography gutterBottom variant="h5" component="div">
                          Image
                        </Typography>
                        {detailPlace.images.map((item_detail_img, index_detail_img) => {
                          return (
                            <CardMedia
                              key={index_detail_img}
                              component="img"
                              height="250"
                              image={item_detail_img}
                              alt="green iguana"
                            />
                          )
                        })}
                      </Card>
                    </Grid>
                  </Grid>
                </Hidden>
                <Hidden smUp>
                  <TabContext value={valueTab}>
                    <Box
                      sx={{
                        borderBottom: 1,
                        borderColor: 'divider',
                        width: '100%'
                      }}
                      style={{ display: 'flex', justifyContent: 'center', marginTop: '15px' }}
                    >
                      <TabList onChange={handleChangeTab} aria-label="lab API tabs example">
                        <Tab label="Information" value="1" />
                        <Tab label="Image" value="2" />
                      </TabList>
                    </Box>
                    <TabPanel value="1">
                      <Card
                        style={{ borderRadius: '6px' }}
                      >
                        <CardMedia
                          component="img"
                          height="250"
                          image={detailPlace.profile_image_url}
                          alt="green iguana"
                        />
                        <CardContent>
                          <Grid container>
                            <Typography gutterBottom variant="h5" component="div">
                              {detailPlace.name}
                            </Typography>
                            <span style={{ color: '#134B8a', fontWeight: 'bold', display: 'flex', justifyContent: 'center', alignItems: 'center', marginLeft: 'auto' }}>
                              <FiberManualRecordIcon /> {detailPlace.rating}
                            </span>
                          </Grid>
                          <Grid container style={{ fontSize: '13px' }}>
                            <Grid item md={3} xs={12} style={{ fontSize: '13px' }}>
                              <Typography gutterBottom component="div" style={{ fontSize: '13px', fontWeight: 'bold' }}>
                                Address :
                              </Typography>
                            </Grid>
                            <Grid item md={9} xs={12} >
                              <Typography gutterBottom component="div" style={{ fontSize: '13px' }}>
                                {detailPlace.address}
                              </Typography>
                            </Grid>
                            <Grid container style={{ fontSize: '15' }}>
                              <Grid item md={3} xs={12}>
                                <Typography gutterBottom component="div" style={{ fontSize: '13px', fontWeight: 'bold' }}>
                                  Opening Hour :
                                </Typography>
                              </Grid>
                              <Grid item md={9} xs={12}>
                                <Typography gutterBottom component="div" style={{ fontSize: '13px' }}>
                                  {detailPlace.operation_time.map((item_open, index_open) => {
                                    return (
                                      <div>
                                        {item_open.day}: {item_open.time_open === 'closed' ? 'Closed' : `${item_open.time_open} AM`} {item_open.time_open === 'closed' ? '' : `- ${item_open.time_close} PM`}
                                      </div>
                                    )
                                  })}
                                </Typography>
                              </Grid>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Card>
                    </TabPanel>
                    <TabPanel value="2">
                      <Card
                        style={{
                          borderRadius: '6px',
                          padding: '15px'
                        }}
                      >
                        <Typography gutterBottom variant="h5" component="div">
                          Image
                        </Typography>
                        {detailPlace.images.map((item_detail_img, index_detail_img) => {
                          return (
                            <CardMedia
                              key={index_detail_img}
                              component="img"
                              height="250"
                              image={item_detail_img}
                              alt="green iguana"
                            />
                          )
                        })}
                      </Card>
                    </TabPanel>
                  </TabContext>
                </Hidden>
              </React.Fragment>
            }
          </Grid>
          {detailPlace.length === 0 ?
            <Grid container style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
              <Stack spacing={5}>
                <Pagination
                  count={totalPage}
                  page={currentPage}
                  variant="outlined"
                  onChange={handleChangePage}
                />
              </Stack>
            </Grid> : ''
          }
        </Grid>
      </Grid>
    </div >
  )
}

export default PlaceList